from django.views.generic import TemplateView
from rest_framework import authentication, permissions, generics, viewsets, status

from .models import Task
from .serializers import TaskSerializer


class Home(TemplateView):
    template_name = "index.html"

	
class TaskViewSet(viewsets.ModelViewSet):
	queryset = Task.objects.all()
	serializer_class = TaskSerializer
	model = Task
