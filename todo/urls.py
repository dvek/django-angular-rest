from django.conf.urls import patterns, include, url
from rest_framework import routers
from .views import Home, TaskViewSet


router = routers.DefaultRouter()
router.register(r'tasks', TaskViewSet)


urlpatterns = [
    url(r'^$', Home.as_view()),
	url(r'^api/v1/', include(router.urls)),
]