'use strict';

angular
.module('demo', ['ngRoute', 'ngCookies', 'restangular'])
  .run(['$http', '$cookies', function ($http, $cookies) {
		
  }])
  .config(['$interpolateProvider', 'RestangularProvider', '$httpProvider', 
  		function($interpolateProvider, RestangularProvider, $httpProvider) {
	  
	  $httpProvider.defaults.xsrfCookieName = 'csrftoken';
	  $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
	  
	  $interpolateProvider.startSymbol('[[');
	  $interpolateProvider.endSymbol(']]');
	  RestangularProvider.setBaseUrl('http://localhost:9090/api/v1');
	  RestangularProvider.setRequestSuffix('/');
  }])
  .service('tasks', ['Restangular', function(Restangular) {
	  var tasks = {
		  getTasks: function() {
		  	return Restangular.all('tasks').getList()
		  }
	  } 
	  return tasks; 	
  }])
  .controller('AppController', ['$scope', 'tasks', function($scope, tasks) {
	  $scope.tasks = tasks.getTasks().then(function(data) {
		  $scope.tasks = data;
	  }, function(response) {
	  	  console.log(response.status);
	  });
	  
	  $scope.add = function(task) {
		  if(task && task.name) {
			  $scope.tasks.post({'name': task.name}).then(function(data) {
				  $scope.tasks.push(data);
				  $scope.taskTemp = null;
			  })
		  }
	  }
	  
	  $scope.update = function(task) {
		  console.log('update');
		  if(task.name) {
		  	  task.put();
		  }
	  }
	  
	  $scope.delete = function(task) {
	  	task.remove().then(function() {
	  		$scope.tasks = _.without($scope.tasks, task)
	  	})
	  }
  }]);

